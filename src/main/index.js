/* eslint-disable */
/* eslint-disable semi */
import { app, BrowserWindow } from 'electron'
const fetch = require('node-fetch');

// USDA API request, simply. ndbno number is food code but there's an actual url
// you can use and just type the food name TBD
var apiKey = 'okMfctc7A0CpYpE0mwgawegMr0wUf34wXvdNBHru';
var ndbno = '01009';
var type = 'b';
var format = 'json';

var url = 'http://api.nal.usda.gov/ndb/reports/?ndbno=' + ndbno + '&type=' + type + '&format' + format + '&api_key=' + apiKey;

fetch(url)
  .then(function (response) {
    return response.json();
  })
  .then(function (myJson) {
    // console.log(JSON.stringify(myJson));
    // do stuff with JSON
});

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */

   // taken out temporarily due to persistent window size
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
  })

  // commenting this out still draws the window
  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */

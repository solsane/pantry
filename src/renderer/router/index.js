import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import settings from '@/components/settings'
import favorites from '@/components/favorites'
import recipe from '@/components/recipe'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/settings',
      name: 'settings',
      component: settings
    },
    {
      path: '/favorites',
      name: 'favorites',
      component: favorites
    },
    {
      path: '/recipe',
      name: 'recipe',
      component: recipe,
      props: (route) => ({ recipeid: route.query.id })
    }
  ]
})

![Pantry Logo](/img/pantry-logo.png)

An electron-vue project that will generate recipes for you based on the ingredients you have catalogged in your personal pantry. 

#### Using Pantry
Take a few moments to catalog some of your ingredients on the "Pantry" that you currently have. Your suggested recipe results will then automatically populate in the "Results" pane.
![screenshot](/img/screenshot.png)
After looking through your results, you may want to take a look at the ingredients you currently have logged in your Pantry.

To do this, click "Add Your Own". In this pane, you have the ability to not just see your list of ingredients, but also add your own ingredients either manually with the input box or via a file upload using the "File" button. You may also delete any of your ingredients by clicking the "Delete" button next to the corresponding ingredient if you so choose to do so.
![screenshot2](/img/screenshot2.png)

#### Build Setup

``` bash
# install dependencies
npm install

# To run the application in the dev environment
npm run dev

# lint all JS/Vue component files in `src/`
npm run lint

# run unit tests
npm test
```

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).

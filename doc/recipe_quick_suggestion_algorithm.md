# Algorithm for recipe quick Suggestion

The proposed algorithm for quick recipe suggestions is the following: where W = weight.

W1 * recipe_rating + W2 * similarity to favorited recipes - W3 * how recently it has been made (0 after 7 days)